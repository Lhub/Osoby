# Osoby

Osoby (nazwa robocza) to fork Known, zawierający zintegrowane wybrane dodatki / pluginy (np. Mastodon) oraz polską lokalizację (na razię samego Known, trwają prace nad lokalizacją dodatków).

Co to jest? Platforma blogowo-socialowa, dla grup lub pojedynczych osób (specjalny tryb jednej osoby autorskiej).
Może działać jako serwis publiczny (wtedy widoczność treści - publiczna/osoby członkowskie/prywatna wybieramy per-post), tylko dla osób członkowskich lub zamknięty.

Nie wywodzi się z Fediverse lecz z Indieweb. Niemniej istniejące wtyczki pozwalają na publikowanie treści na Mastodonie (pracujemy nad wtyczką) i Diasprze.

Podstawowe typy treści jakie pozwala redagować to Status, Post, Foto, Audio, Zakładka, Wydarzenie i RSVP. To ostatnie, to odpowiedź na Wydarzenie (Będę, Może, Nie będzie mnie) - répondez s’il vous plaît (dosłownie: proszę odpowiedzieć). Do dystrybucji Osoby dołączamy również wtyczkę umożliwiającą publikację Wideo.

Szerszy opis aplikacji niebawem.